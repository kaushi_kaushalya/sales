@include('includes.head')

    <div class="container">
        <div class="mb-3">
            <h1>Login Form</h1>
        </div>
        <form method="POST" action="{{ url('/login') }}">
        @csrf <!-- {{ csrf_field() }} -->
            <div class="col-sm-10">
                <div class="col-sm-7">
                    <label for="exampleFormControlInput1" class="form-label">Email address</label>
                    <input type="email" class="form-control" id="email" name="email" placeholder="name@example.com">
                    @if ($errors->has('user_email'))
                        <span class="invalid-feedback">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                    @endif
                </div>
                <div class="col-sm-7">
                    <label for="exampleFormControlInput1" class="form-label">Password</label>
                    <input type="password" class="form-control" id="password" name="password">
                    @if ($errors->has('password'))
                        <span class="invalid-feedback">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                    @endif
                </div>
                <div class="col-sm-7">
                <br/>
                    <button type="submit" class="btn btn-primary">{{ __('Login') }}</button>
                </div>
            </div>
        </form>
    </div>
@include('includes.footer')