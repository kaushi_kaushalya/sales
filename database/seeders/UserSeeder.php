<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use \Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate();
        $users = [
            ['name' => 'admin', 'email' => 'kaushi5054@gmail.com', 'password' => bcrypt('kaushi5054'), 'remember_token' => '','created_at' => now()->toDateTimeString()],
        ];
        DB::table('users')->insert($users);
    }
}
