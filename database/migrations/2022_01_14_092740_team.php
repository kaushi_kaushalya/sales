<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Team extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('team', function (Blueprint $table) {
            $table->increments('id')->unsigned()->nullable(false);
            $table->string('first_name',100)->default(null)->nullable();
            $table->string('last_name',100)->default(null)->nullable();
            $table->string('email')->unique();
            $table->string('mobile')->unique();
            $table->string('rout');
            $table->string('joined');
            $table->tinyInteger('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
