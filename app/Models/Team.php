<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $table   = 'team';

    const ACTIVE_STATUS   = 1;
    const INACTIVE_STATUS = 2;

    protected $fillable = ['id', 'first_name', 'last_name', 'email', 'mobile', 'rout', 'joined', 'status', 'updated_at', 'created_at' ];
}
