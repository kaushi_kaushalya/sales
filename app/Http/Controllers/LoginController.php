<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class LoginController extends Controller
{
    /**
     * get login from
     */
    public function index()
    {
        return view('login'); 
    }

    /**
     * log to the server
     */
    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        # get data
        $userData = [
            'email'    => $request->get('email'),
            'password' => $request->get('password')
        ];
        # check auth
        if(Auth::attempt($userData)){ 
            return redirect('/dashboard');
        }
        return back()->with('error', 'Wrong Login');

    }

    /**
     * logout
     */
    public function logout()
    {
        Auth::logout();
        return view('login'); 
    }
}
