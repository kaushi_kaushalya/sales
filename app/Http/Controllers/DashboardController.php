<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Team;

class DashboardController extends Controller
{
    /**
     * get index details
     */
    public function index(Request $request)
    {
       # get tem members 
       $team = Team::select('*')->get()->toArray();
        return view('dashboard', ['team' => $team] ); 
    }
}
